package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    //ToDo: Complete me
    public AttackWithGun() {}

    public String attack() {
        return "Shot you with a gun!";
    }

    public String getType() {
        return "Gun";
    }


}
