package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me

    public String defend() {
        return "Defending with barrier!";
    }

    public String getType() {
        return "Barrier";
    }
}
