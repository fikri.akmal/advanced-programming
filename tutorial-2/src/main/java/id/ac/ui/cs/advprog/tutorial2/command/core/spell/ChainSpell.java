package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me

    protected ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        // TODO
        for (Spell spell: spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        // TODO
        for (Spell spell: spells) {
            spell.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
